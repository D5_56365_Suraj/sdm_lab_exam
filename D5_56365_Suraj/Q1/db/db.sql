
CREATE TABLE movie (
  Id INTEGER PRIMARY KEY auto_increment,
  movie_title VARCHAR(100),
  movie_release_date date(300),
  movie_time time,
    director_name VARCHAR(100)
);
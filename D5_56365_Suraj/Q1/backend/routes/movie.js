const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (request, response) => {
    const query = `
      SELECT * from movie
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  

router.post('/', (request, response) => {
  const { movie_title, movie_release_date ,movie_time,director_name  } = request.body

  const query = `
    INSERT INTO movie
      (movie_title, movie_release_date, movie_time, director_name)
    VALUES
      ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})


router.put('/:id', (request, response) => {
  const { id } = request.params
  const { movie_release_date, movie_time } = request.body

  const query = `
    UPDATE movie
    SET
    movie_release_date = '${movie_release_date}', 
      movie_time = '${movie_time}'
    WHERE
      Id = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:id', (request, response) => {
  const { id } = request.params

  const query = `
    DELETE FROM movie
    WHERE
      Id = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
